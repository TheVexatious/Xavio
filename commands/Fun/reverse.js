module.exports = {
  cooldown: 2,
  usage: "<text>",
  description: "Reverses the text! put `-u` at the end to enable reversing mentions and channels",
  process(bot, msg, suffix){
    if(suffix){
      let args = suffix.split(" ");
      if(args[args.length - 1].toLowerCase() == "-u"){
        args.splice(args.length - 1, 1);
        msg.channel.createMessage(`\u202e${args.join(" ")}`);
      }else{
        msg.channel.createMessage(suffix.split("").reverse().join(""));
      }
    }else{
      bot.util.correctUsage("reverse", bot.config.prefix, this.usage, msg);
    }
  }
};
