const request = require("request");

function parseMemeChars(text) {
	text = text.replace(/-/g, "--");
	text = text.replace(/ /g, "-");
	text = text.replace(/_/g, "__");
	text = text.replace(/\?/g, "~q");
	text = text.replace(/%/g, "~p");
	text = text.replace(/#/g, "~h");
	text = text.replace(/\//g, "~s");
	text = text.replace(/''/g, "\"");
	return text;
}

module.exports = {
	usage: "<img url>  '<top text>' '<bottom text>'",
	example: "custommeme<http://i.imgur.com/Tsj9uG0.jpg> 'be strong' 'hide the pain'",
	aliases: "cmeme",
	cooldown: 5,
	description: "Generates a meme with given top and bottom text",
	process(bot, msg, suffix){
		let rgs = suffix.split("'");
		rgs[0] = rgs[0].replace(" ", "");
		let res = ("http://memegen.link/custom/" + String(parseMemeChars(rgs[1])) + "/" + String(parseMemeChars(rgs[3])) + ".jpg?alt=" + String((rgs[0])) + "&font=impact");
		if(rgs.length != 5){
			bot.util.correctUsage("custommeme", bot.config.prefix, this.usage, msg);
		}else{
			request({uri: res, encoding: null}, (err, resp, body)=>{
					bot.createMessage(msg.channel.id, "", {
							name: "meme.png",
							file: body
					});
			});
		}
	}
};
