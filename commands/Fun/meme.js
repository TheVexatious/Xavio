const Imgflipper = require("imgflipper");

const memes = {
	onedoesnot: 61579,
	everywhere: 347390,
	doge: 8072285,
	grumpycat: 405658,
	successkid: 61544,
	yuno: 61527,
	ancientaliens: 101470,
	interestingman: 61532,
	cheers: 5496396,
	braceyourselves: 61546,
	problems: 61539,
	badluckbrian: 61585,
	allthe: 61533,
	futurama: 61520,
	waitingskeleton: 4087833,
	disastergirl: 97984,
	grandmafindstheinternet: 61556,
	grandmainternet: 61556,
	awkwardmomentsealion: 13757816,
	awkwardmoment: 13757816,
	"10guy": 101440,
	badpundog: 12403754,
	badpun: 12403754,
	illjustwait: 109765,
	illjustwaithere: 109765,
	yodwagheardyou: 101716,
	yodawg: 101716,
	facepalm: 1509839,
	thirdworldskepticalkid: 101288,
	skepticalkid: 101288,
	therockdriving: 21735,
	rockdriving: 21735,
	eviltoddler: 235589,
	seenobodycares: 6531067,
	nobodycares: 6531067,
	thirdworldsuccesskid: 101287,
	successkid2: 101287,
	aintnobodygottimeforthat: 442575,
	angtft: 442575
};

module.exports = {
  usage: "<memetype> 'top text' 'bottom text'",
	example: "meme everywhere 'Memes' 'Memes Everywhere!'",
  cooldown: 5,
  description: "Generates a meme with the given information",
  process(bot, msg, suffix) {
		if(!suffix || suffix.split("'").length <= 3) return bot.util.correctUsage("meme", bot.config.prefix, this.usage, msg);
    let tags = suffix.split("'");
    let memetype = suffix.split(" ")[0];
    const imgflipper = new Imgflipper(bot.config.imgflipperUser, bot.config.imgflipperPass);
    imgflipper.generateMeme(!isNaN(memetype) ? memetype : memes[memetype], tags[1] ? tags[1] : "", tags[3] ? tags[3] : "", (error, image) => {
      if(image){
        msg.channel.createMessage(image);
      }else{
				if(error) console.log(error, `\nStatus Code: ${response.statusCode}`)
        msg.channel.createMessage("Uh-oh! I ran into an error!");
      }
    });
  }
};

// TODO: doc this
