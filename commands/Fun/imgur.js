const request = require("request");

module.exports = {
  cooldown: 5,
  description: "Sends an image from the specified imgur gallery",
  usage: "<subreddit> [--nsfw] [--day | --week | --month | --year | --all] [number of images 1 - 5 (optional)]",
  example: ["imgur awwnime", "imgur boobies --nsfw 5"],
  process(bot, msg, suffix){
    if(!suffix) return bot.util.correctUsage("imgur", bot.config.prefix, this.usage, msg);
    if (/[\uD000-\uF8FF]/g.test(suffix)) return msg.channel.createMessage("Your search cannot contain unicode characters!");
    if(suffix && /^[^-].*/.test(suffix)){
      const time = (/(--day|--week|--month|--year|--all)/i.test(suffix)) ? /(--day|--week|--month|--year|--all)/i.exec(suffix)[0] : "--week";
      const sendNSFW = (/ ?--nsfw/i.test(suffix)) ? true : false;
      if(sendNSFW){
        if(!(msg.channel.name.indexOf("nsfw") > -1)){
          bot.createMessage(msg.channel.id, "**Sorry but this channel does not allow NSFW commands!**");
          return;
        }
      }
      const saf = suffix;
      const safs = saf.split(" ");
      const safl = Number(safs.length) - 1;
      const safla = safs[safl];
      if(!isNaN(safla)){
        const ss = suffix.split(" ");
        const sl = ss.splice(2, 1);
        const sj = ss.join(" ");
        suffix = sj;
      }
      request({
        url: `https://api.imgur.com/3/gallery/r/${suffix.replace(/(--day|--week|--month|--year|--all|--nsfw|\/?r\/| )/gi, "")}/top/${time.substring(2)}/50`,
        headers: {"Authorization": "Client-ID " + bot.config.imgurClientId}
      }, (error, response, body) => {
        if(error || response.statusCode !== 200){
          bot.createMessage(msg.channel.id, "Uh-oh! I ran into an error!");
        }else if(body){
          body = JSON.parse(body);
          if(body.hasOwnProperty("data") && body.data !== undefined && body.data.length !== 0){
            if(safla && !isNaN(safla) && parseInt(safla) <= 5){
              for(var i = 0; i < parseInt(safla); i++){
                let toSend = body.data[Math.floor(Math.random() * (body.data.length))];
                if(!sendNSFW && !toSend.nsfw) {
                  if(toSend.title){
                    bot.createMessage(msg.channel.id, toSend.link+"\n"+toSend.title);
                  }else{
                    bot.createMessage(msg.channel.id, toSend.link);
                  }
                }else if(sendNSFW && toSend.nsfw == true) {
                  if(toSend.title){
                    bot.createMessage(msg.channel.id, toSend.link+" **(NSFW)**\n"+toSend.title);
                  }else{
                    bot.createMessage(msg.channel.id, toSend.link+" **(NSFW)**");
                  }
                }
              }
            }else{
              let toSend = body.data[Math.floor(Math.random() * (body.data.length))];
              if(!sendNSFW && !toSend.nsfw) {
                if(toSend.title){
                  bot.createMessage(msg.channel.id, toSend.link+"\n"+toSend.title);
                }else{
                  bot.createMessage(msg.channel.id, toSend.link);
                }
              }else if(sendNSFW && toSend.nsfw == true) {
                if(toSend.title){
                  bot.createMessage(msg.channel.id, toSend.link+" **(NSFW)**\n"+toSend.title);
                }else{
                  bot.createMessage(msg.channel.id, toSend.link+" **(NSFW)**");
                }
              }
            }
          } else bot.createMessage(msg.channel.id, "No results!");
        }
      });
    }else bot.util.correctUsage("imgur", bot.config.prefix, this.usage, msg);
  }
};
