module.exports = {
  cooldown: 2,
  description: "Flips a coin and tells if it was a head or tail",
  process(bot, msg){
    const i = ~~(Math.random()*2);
    const options = ["head", "tail"];
    msg.channel.createMessage(`It's a **${options[i]}**!`);
  }
};
