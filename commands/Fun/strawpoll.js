const request = require("request");

module.exports = {
  cooldown: 10,
  usage: "[title] | <option1>, <option2>, [option3, ...]",
  process(bot, msg, suffix){
    if(suffix && /^[^, ](.*), ?(.*)[^, ]$/.test(suffix)){
      const title = suffix.split(/ ?\| ?/)[0];
      suffix = suffix.split(/ ?\| ?/)[1].split(/, ?/);
      request.post({
        url: "https://strawpoll.me/api/v2/polls",
        headers: {"content-type": "application/json"},
        json: true,
        body: {
          "title": title,
          "options": suffix
        },
        followAllRedirects: true
      }, (error, response, body) => {
        if(response.statusCode == 200){
          bot.createMessage(msg.channel.id, msg.author.username.replace(/@/g, "@\u200b")+" created a strawpoll. Vote here: <http://strawpoll.me/"+body.id+">");
        } else {
          msg.channel.createMessage("Uh-oh! I ran into an error!");
        }
      });
    } else bot.util.correctUsage("strawpoll", bot.config.prefix, this.usage, msg);
  }
};
