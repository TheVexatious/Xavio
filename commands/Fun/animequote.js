const aq = require("animequote");

module.exports = {
  cooldown: 3,
  description: "Sends a random anime quote along with the character and anime which it came from",
  process(bot, msg){
    const quote = aq();
    msg.channel.createMessage(`***${quote.quotesentence}***
 *-${quote.quotecharacter} (from ${quote.quoteanime})*`);
  }
};
