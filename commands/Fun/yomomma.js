const request = require("request");

module.exports = {
  cooldown: 2,
  description: "Tells a yomomma joke",
  aliases: ["yomama", "yomamma"],
  process(bot, msg){
    request("http://api.yomomma.info/", (error, response, body) => {
      if (error || response.statusCode !== 200) {
        msg.channel.createMessage("Uh-oh! I ran into an error!");
      }else{
        try{
          msg.channel.createMessage(JSON.parse(body).joke);
        }catch(e){
          msg.channel.createMessage("Uh-oh! I ran into an error!");
        }
      }
    });
  }
};
