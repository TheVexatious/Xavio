module.exports = {
  cooldown: 5,
  description: "Searches for how many people in the server play the game you queried or are playing",
  usage: "<name>",
  process(bot, msg, suffix) {
    if(!suffix && (msg.author.game.name == null || msg.author.game.name == "" || !msg.author.game)){
      return bot.util.correctUsage("whoplays", bot.config.prefix, this.usage, msg);
    }
    const name = suffix ? suffix : msg.author.game.name;
    let toSend;
    let members = msg.channel.guild.members.filter(e => e.game ? e.game.name == name : false);
    let names = members.map(e => `${e.user.username}#${e.user.discriminator}`);
    if(members.length > 25){
      toSend = `**List of people who play __${name}__**

${names.slice(0,25).join("\n")}

...And ${names.length - 25} more!`;

    }else if(members.length > 0){
      toSend = `**List of people who play __${name}__**

${names.join("\n")}`;
    }else{
      toSend = `Nobody in this guild plays "**${name}**"`;
    }

    msg.channel.createMessage(toSend);
  }
};
