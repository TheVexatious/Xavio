const request = require("request");

module.exports = {
  cooldown: 3,
  usage: "<term>",
  aliases: "urban",
  descripiton: "Searches through Urban Dictionary, the worst Dictionary on the internet!",
  process(bot, msg, suffix) {
    if (suffix) {
      request(`http://api.urbandictionary.com/v0/define?term=${suffix}`, (error, response, body) => {
        if (!error && response.statusCode == 200) {
          let uD = JSON.parse(body);
          if (uD.result_type !== "no_results") {
            const item = uD.list[0];
            msg.channel.createMessage({
              embed: {
                title: item.word,
                description: item.permalink,
                color: xavio.colors.DEFAULT,
                fields: [{
                    name: "Definition",
                    value: item.definition,
                    inline: false
                  },
                  {
                    name: "Example",
                    value: item.example,
                    inline: false
                  },
                  {
                    name: ":thumbsup:",
                    value: item.thumbs_up.toString(),
                    inline: true
                  },
                  {
                    name: ":thumbsdown:",
                    value: item.thumbs_down.toString(),
                    inline: true
                  }
                ]
              }
            });
          } else {
            bot.createMessage(msg.channel.id, `Could not find any results for "${suffix}"`);
          }
        } else {
          msg.channel.createMessage("Uh-oh! I ran into an error!");
        }
      });
    } else {
      bot.util.correctUsage("urban", bot.config.prefix, this.usage, msg);
    }
  }
};
