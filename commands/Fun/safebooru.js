const request = require("request");

module.exports = {
  cooldown: 2,
  usage: "<search-terms>",
  description: "Sends an image from safebooru",
  process(bot, msg, suffix){
    request(`http://safebooru.org/index.php?page=dapi&s=post&q=index&json=1&limit=10${suffix ? "&tags=" + suffix : "&pid=1"}`, (error, response, body) => {
      if(error || response.statusCode !== 200){
        msg.channel.createMessage("Uh-oh! I ran into an error!");
      }else{
        try{
          const data = JSON.parse(body);
          const pic = data[Math.floor(Math.random() * data.length)];
          bot.createMessage(msg.channel.id, "http://safebooru.org/images/"+pic.directory+"/"+pic.image);
        }catch(e){
          bot.createMessage(msg.channel.id, `No results for "${suffix}"!`);
        }
      }
    });
  }
};
