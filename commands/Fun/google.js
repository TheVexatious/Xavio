const google = require("google");

module.exports = {
  cooldown: 5,
  description: "Searches your search terms through google and returns the first result",
  usage: "<search-terms>",
  example: "google Discord",
  process(bot, msg, suffix) {
    if (suffix) {
      google(suffix, (error, response) => {
        if (error || !response || !response.links || response.links.length < 1) {
          msg.channel.createMessage(`No results for "${suffix}"`);
        } else {
          const links = response.links.filter(e => e.link !== null);
          const fields = [];
          links.splice(0,5).forEach((e, i) => {
            fields.push({
              name: `#${i+1}. ${e.title}`,
              value: `${e.description}\n${e.link}${i === 0 ? "\n\n" : ""}`
            });
          });
          msg.channel.createMessage({
            embed: {
              author: {
                name: "Google",
                icon_url: "http://i.imgur.com/YYSqT7v.jpg"
              },
              color: xavio.colors.DEFAULT,
              fields: fields
            }
          });
        }
      });
    } else {
      bot.util.correctUsage("google", bot.config.prefix, this.usage, msg);
    }
  }
};
