const request = require("request");

module.exports = {
  description: "Searches wikipedia for the given search terms",
  cooldown: 5,
  process(bot, msg, suffix) {
    if (!suffix) {
      return bot.util.correctUsage("8ball", bot.config.prefix, this.usage, msg);
    }
    request(`https://en.wikipedia.org/w/api.php?action=opensearch&search=${suffix.replace(/&/g, "")}&limit=1&namespace=0&format=json`, (error, response, body) => {
      if (error) {
        return bot.createMessage(msg.channel.id, "Uh-oh! got an error: " + error.message + " status code: " + response.statusCode);
      }
      var data = JSON.parse(body);
      if (!data || !data[1] || data[1] == "" || data[1] == undefined) {
        return bot.createMessage(msg.channel.id, `No results for \"${suffix}\"`);
      }
      msg.channel.createMessage({
          embed: {
            title: data[1][0],
            url: data[3][0],
            description: `${data[2][0]}\n\n${data[3][0]}`,
            color: xavio.colors.DEFAULT
          }
        });
    });
  }
};
