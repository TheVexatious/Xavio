const request = require("request");

module.exports = {
  cooldown: 3,
  description: "Sends a random gif",
  usage: "<searchTerms>",
  example: "gif cat",
  process: function(bot, msg, suffix){
    request(`http://api.giphy.com/v1/gifs/random?api_key=${bot.config.giphyApiKey}${suffix ? "&tag="+suffix : ""}`, (error,response,body) => {
      if(error){
        bot.createMessage(msg.channel.id, `Got an error: ${error} status code: ${response.statusCode}`);
      }else{
        const gif = JSON.parse(body);
        bot.createMessage(msg.channel.id, gif.data.url);
      }
    });
  }
};
