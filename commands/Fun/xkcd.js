const request = require("request");

module.exports = {
  cooldown: 3,
  usage: "<comic-number>",
  description: "Sends a random (or chosen) xkcd comic",
  process(bot, msg, suffix){
    request("http://xkcd.com/info.0.json", (error, response, body) => {
      if(!error && response.statusCode == 200){
        let xkcdInfo = JSON.parse(body);
        if(suffix){
          let isnum = /^\d+$/.test(suffix);
          if(isnum){
            if([suffix] < xkcdInfo.num){
              request("http://xkcd.com/"+suffix+"/info.0.json", (error, response, body) => {
                if(!error && response.statusCode == 200){
                  xkcdInfo = JSON.parse(body);
                  bot.createMessage(msg.channel.id, xkcdInfo.img);
                }else{
                  console.log("Got an error: "+error+" status code: "+respose.statusCode);
                }
              });
            }else{
              bot.createMessage(msg.channel.id, "There are only "+xkcdInfo.num+" xkcd comics!");
            }
          }else{
            bot.createMessage(msg.channel.id, xkcdInfo.img);
          }
        }else{
          let xkcdRandom = Math.floor(Math.random() * (xkcdInfo.num - 1)) + 1;
          request("http://xkcd.com/"+xkcdRandom+"/info.0.json", (error, response, body) => {
            if (!error && response.statusCode == 200){
              xkcdInfo = JSON.parse(body);
              bot.createMessage(msg.channel,xkcdInfo.img);
            }else{
              console.log("Got an error: "+error+" status code: "+response.statusCode);
            }
          });
        }
      }else{
        console.log("Got an error: "+error+" status code: "+response.statusCode);
      }
    });
  }
};
