const locales = {
  afrikaans: "af",
  albanian: "sq",
  arabic: "ar",
  armenian: "hy",
  azerbaijan: "az",
  bashkir: "ba",
  basque: "eu",
  belarusian: "be",
  bengali: "bn",
  bosnian: "bs",
  bulgarian: "bg",
  catalan: "ca",
  chinese: "zh",
  croatian: "hr",
  czech: "cs",
  danish: "da",
  dutch: "nl",
  english: "en",
  estonian: "et",
  finish: "fi",
  french: "fr",
  galician: "gl",
  georgian: "ka",
  german: "de",
  greek: "el",
  gujarati: "gu",
  haitian: "ht",
  creole: "ht",
  hebrew: "he",
  hindi: "hi",
  hungarian: "hu",
  icelandic: "is",
  indonesian: "id",
  irish: "ga",
  italian: "it",
  japanese: "ja",
  kannada: "kn",
  kazakh: "kk",
  korean: "ko",
  kyrgyz: "ky",
  latin: "la",
  latvian: "lv",
  lithuanian: "lt",
  macedonian: "mk",
  malagasy: "mg",
  malay: "ms",
  maltese: "mt",
  mongolian: "mn",
  norwegian: "no",
  persian: "fa",
  polish: "pl",
  portuguese: "pt",
  punjabi: "pa",
  romanian: "ro",
  russian: "ru",
  serbian: "sr",
  sinhala: "si",
  slovakian: "sk",
  slovenian: "sl",
  spanish: "es",
  swahili: "sw",
  swedish: "sv",
  tagalog: "tl",
  tajik: "tg",
  tamil: "ta",
  tatar: "tt",
  thai: "th",
  turkish: "tr",
  udmurt: "udm",
  ukrainian: "uk",
  urdu: "ur",
  uzbek: "uz",
  vietnamese: "vi",
  welsh: "cy"
};

const request = require("request");

module.exports = {
  cooldown: 5,
  description: "Sends the given text translated to the specified language code (go to <http://goo.gl/Krz5r3> to get a list of language codes)",
  usage: "<lang-code> [text]",
  process(bot, msg, suffix) {
    if(bot.config.yandexTranslateKey == "" || !bot.config.hasOwnProperty("yandexTranslateKey")){
      return msg.channel.createMessage("Yandex API Translate Key not provided by the bot owner!");
    }
    if (suffix) {
      if (suffix.split(" ").length > 1) {
        const lang = suffix.split(" ")[0].length == 2 ? suffix.split(" ")[0] : (Object.keys(locales).indexOf(suffix.split(" ")[0]) > -1 ? locales[suffix.split(" ")[0]] : null);
        const text = suffix.split(" ").slice(1).join(" ");
        if(lang !== null){
          request(`https://translate.yandex.net/api/v1.5/tr.json/translate?key=${bot.config.yandexTranslateKey}&text=${text}&lang=${lang}`, (error, response, body) => {
            if(error || response.statusCode !== 200) {
              msg.channel.createMessage("Uh-oh! I ran into an error!");
            } else {
              const data = JSON.parse(body);
  						if(data.code == "200" || data.code == 200){
  							bot.createMessage(msg.channel.id, data.text[0]);
  						}else{
  							if(data.message){
  								bot.createMessage(msg.channel.id, "Got response code: `"+data.code+"` and recieved message `"+data.message+"`");
  							}else{
  								bot.createMessage(msg.channel.id, "Got response code `"+data.code+"`");
  							}
  						}
            }
          });
        }else{
          msg.channel.createMessage("Please use a language code from <http://goo.gl/Krz5r3>");
        }
      } else {
        bot.util.correctUsage("translate", bot.config.prefix, this.usage, msg);
      }
    } else {
      bot.util.correctUsage("translate", bot.config.prefix, this.usage, msg);
    }
  }
};
