module.exports = {
  ownerOnly: true,
  hidden: true,
  aliases: "$",
  process(bot, msg, suffix, plugins) {
    if (!suffix) return msg.channel.createMessage("Try again with some input!");
    let result = "No result!";
    try {
      result = eval(suffix);
    } catch(e) {
      result = e;
    }
    msg.channel.createMessage(`**Input:**
\`\`\`js
${suffix}
\`\`\`
**Output:**
\`\`\`js
${result}
\`\`\`
      `);
  }
};
