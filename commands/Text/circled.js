const toUnicode = require("to-unicode");

module.exports = {
  cooldown: 2,
  usage: "<text>",
  description: "Sends the text in circled format",
  process(bot, msg, suffix){
    if(suffix){
      msg.channel.createMessage(toUnicode(suffix, "circled"));
    }else bot.util.correctUsage("circled", bot.config.prefix, this.usage, msg);
  }
};
