const map = {
  a: 4,
  b: 8,
  e: 3,
  g: 6,
  l: 1,
  o: 0,
  s: 5,
  t: 7,
  "æ": 43,
  "ø": 03,
  "å": 44
};

module.exports = {
  cooldown: 2,
  usage: "<text>",
  description: "Converts the text to a 1337 text",
  process(bot, msg, suffix){
    if(suffix){
      let string = suffix;
      string = string.replace(/cks/g, "x");
      for (const letter in map) {
        if (map.hasOwnProperty(letter)) {
          string = string.replace(new RegExp(letter, "g"),map[letter]);
        }
      }
      msg.channel.createMessage(string.toUpperCase());
    }else{
      bot.util.correctUsage("leet", bot.config.prefix, this.usage, msg);
    }
  }
};
