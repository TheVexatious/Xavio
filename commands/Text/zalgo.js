const zalgo = require("to-zalgo");

module.exports = {
  cooldown: 3,
  usage: "<text>",
  descrpition: "Sends the given text in xalgo format",
  process(bot, msg, suffix){
    if(suffix){
      msg.channel.createMessage(zalgo(suffix));
    } else bot.util.correctUsage("zalgo", bot.config.prefix, this.usage, msg);
  }
};
