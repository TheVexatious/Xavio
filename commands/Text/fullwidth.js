module.exports = {
  cooldown: 2,
  usage: "<text>",
  description: "Sends the text in fullwidth!",
  process(bot, msg, suffix){
    if(suffix){
      bot.createMessage(msg.channel.id, suffix.replace(/[a-zA-Z0-9!\?\.'";:\]\[}{\)\(@#\$%\^&\*\-_=\+`~><]/g, (c) => String.fromCharCode(0xFEE0 + c.charCodeAt(0))).replace(/ /g, "　"));
    }else bot.util.correctUsage("fullwidth", bot.config.prefix, this.usage, msg);
  }
};
