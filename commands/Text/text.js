module.exports = {
  cooldown: 2,
  usage: "<text> <type>",
  description: "Converts the text",
  process(bot, msg, suffix, plugins){
    if(suffix){
      const arr = suffix.split(" ");
      if(arr.length >= 2){
        const type = arr.pop();
        const text = arr.join(" ");
        const converted = plugins.textMod.convert(text, type);
        msg.channel.createMessage(converted == null ? "Please try again with a valid text type" : converted);
      }else bot.util.correctUsage("text", bot.config.prefix, this.usage, msg);
    }else bot.util.correctUsage("text", bot.config.prefix, this.usage, msg);
  }
};
