const flip = require("flip-text");

module.exports = {
  cooldown: 2,
  usage: "<text>",
  description: "Flips the given text!",
  process(bot, msg, suffix){
    if(suffix){
      bot.createMessage(msg.channel.id, flip(suffix));
    }else bot.util.correctUsage("flip", bot.config.prefix, this.usage, msg);
  }
};
