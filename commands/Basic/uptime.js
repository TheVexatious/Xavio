const util = require("../../utils/util.js");

module.exports = {
  tag: "Basic",
  description: "Tells how long the bot has been online for",
  cooldown: 2,
  process(bot, msg, suffix){
    msg.channel.createMessage(`I have been up for **${util.msFormat(bot.uptime)}**`);
  }
};
