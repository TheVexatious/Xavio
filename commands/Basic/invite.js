module.exports = {
  cooldown: 5,
  description: "Sends a link to invite the bot to your server",
  process(bot, msg){
    if(bot.config.clientID){
      bot.createMessage(msg.channel.id, `Use this to bring me to your server: https://discordapp.com/oauth2/authorize?client_id=${bot.config.clientID}&scope=bot`);
    }else{
      msg.channel.createMessage("Cannot send an invite link because the client ID was not specified by the owner!");
    }
  }
};
