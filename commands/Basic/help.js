function sendHelpMessage(bot, msg, suffix, commands, config) {
  if (!suffix) {
    if (msg.channel.guild) {
      bot.createMessage(msg.channel.id, " Alright! Check your PM :thumbsup: ");
    }

    const fields = Object.keys(bot.commandType).filter(e => !e.hidden).map(e => {
        return {
          name: bot.commandType[e].name,
          value: bot.commandType[e].description
        };
    });
    bot.getDMChannel(msg.author.id)
      .then(c => {
        c.createMessage({
          embed: {
            description: "Below are the names and descriptions of Xavio's command categories. To view the commands in each category. Do `x!help [category]`\neg: `x!help basic`",
            color: xavio.colors.DEFAULT,
            author: {
              name: "Xavio Commands",
              icon_url: bot.user.avatarURL
            },
            fields: fields
          }
        });
      });
  } else {
    suffix = suffix.toLowerCase();
    if (bot.commandType.hasOwnProperty(suffix) || Object.keys(bot.commandType).filter(e => bot.commandType[e].name.toLowerCase() == suffix).length >= 1) {
      const type = bot.commandType[Object.keys(bot.commandType).find(e => bot.commandType[e].name.toLowerCase() == suffix)];
      const cmds = Object.keys(commands).filter(e => commands[e].category.name.toLowerCase() == suffix).map(e => {
        if (commands[e].hidden) {
          return;
        } else {
          return {
            name: `\`${commands[e].name} ${commands[e].usage || ""}\``,
            value: commands[e].description
          };
        }
      });
      msg.channel.createMessage({
        embed: {
          title: `Commands: __${type.name}__`,
          description: `${type.description}\n\nTo view more information on each command, do: \`x!describe [command]\``,
          color: type.color,
          fields: cmds
        }
      });
    }else {
      msg.channel.createMessage(`**${msg.author.username}**, "${suffix}" isn't a valid command category!`);
    }
  }
}

module.exports = {
  description: "Sends help message",
  cooldown: 2,
  process(bot, msg, suffix) {
    sendHelpMessage(bot, msg, suffix, bot.commands, bot.config);
  }
};
