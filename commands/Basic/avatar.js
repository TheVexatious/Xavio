module.exports = {
  cooldown: 5,
  description: "Gives you the link to your avatar or the avatar of the person you mentioned",
  guildOnly: true,
  example: ["avatar @\u200bSomeRandomGuy#1337", "avatar"],
  usage: "(@mention)",
  process(bot, msg, suffix) {
    if (suffix) {
      if (msg.mentions && msg.mentions.length > 0){
        msg.channel.createMessage(`Here's your avatar for **${msg.mentions[0].username}**: ${msg.mentions[0].avatarURL}?size=1024`);
      } else {
        const user = bot.util.findUser(suffix, msg.channel.guild.members);
        if(user){
          msg.channel.createMessage({
            embed: {
              title: `Here's your avatar for **${user.username}**`,
              url: `${user.avatarURL}?size=1024`,
              color: xavio.colors.DEFAULT,
              image: {
                url: `${user.avatarURL}?size=1024`
              }
            }
          });
        } else {
          msg.channel.createMessage(`I couldn't find the user "**${suffix}**", please consider using \`@mention\` or typing the exact username/nickname.`);
        }
      }
    } else {
      msg.channel.createMessage(`Here's your avatar, **${msg.author.username}**: ${msg.author.avatarURL}?size=1024`);
      msg.channel.createMessage({
        embed: {
          title: `Here's your avatar, **${msg.author.username}**`,
          url: `${user.avatarURL}?size=1024`,
          color: xavio.colors.DEFAULT,
          image: {
            url: `${msg.author.avatarURL}?size=1024`
          }
        }
      });
    }
  }
};
