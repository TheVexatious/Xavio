module.exports = {
  description: "Gives more information on a command",
  usage: "<commandName>",
  example: "describe ping",
  cooldown: 2,
  process(bot, msg, suffix) {
    if(!suffix) return bot.util.correctUsage("describe", bot.config.prefix, this.usage, msg);
    const {commands} = bot;
    if (commands[suffix]) {
      sendHelp(msg, commands[suffix]);
    } else {
      let found = false;
      for (let key in commands) {
        if (commands[key].aliases && commands[key].aliases.includes(suffix)) {
          found = true;
          sendHelp(msg, commands[key]);
        }
      }
      if (!found) bot.createMessage(msg.channel.id, `${msg.author.username}, There is no such "${suffix}" command!"`);
    }
  }
};

function sendHelp(msg, command) {
  const accept = ["cooldown", "category", "examples", "aliases", "guildOnly", "ownerOnly", "hidden"];
  const fields = Object.keys(command).filter(e => {return accept.includes(e) && !!command[e];}).map(e => {
    if (e === "cooldown") {
      return {
        name: "Cooldown",
        value: `${command[e]} ${command[e] > 1 ? "seconds" : "second"}`
      };
    }else if(e === "category") {
      return {
        name: "Category",
        value: command[e].name
      };
    }else if(e === "examples") {
      return {
        name: "Examples",
        value: command[e] instanceof Array ? ":small_blue_diamond: "+command[e].join("\n:small_blue_diamond: ") : ":small_blue_diamond: "+command[e]
      };
    }else if(e === "aliases") {
      return {
        name: "aliases",
        value: command[e] instanceof Array ? ":small_orange_diamond: "+command[e].join("\n:small_orange_diamond: ") : ":small_orange_diamond: "+command[e]
      };
    }else {
      return {
        name: e,
        value: command[e]
      };
    }
  });
  msg.channel.createMessage({
    embed: {
      title: `\`x!${command.name}${command.usage ? " "+command.usage : ""}\``,
      description: command.description || "No description!",
      color: command.category.color,
      fields: fields
    }
  });
}
