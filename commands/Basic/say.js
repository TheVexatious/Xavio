module.exports = {
  description: "Echoes back the given suffix",
  usage: "<text>",
  cooldown: 2,
  process(bot, msg, suffix){
    if(suffix){
      msg.channel.createMessage(msg.cleanContent.substring(msg.cleanContent.length - suffix.length).trim().replace(/@/g, "@\u200b"));
    }else{
      bot.util.correctUsage("say", bot.config.prefix, this.usage, msg);
    }
  }
};
