const path = require("path");
const pkg = require(path.join(xavio.baseDir, "package.json"));

module.exports = {
  cooldown: 3,
  description: "Sends information about the bot",
  process(bot, msg) {
    msg.channel.createMessage({
      embed: {
        title: `Xavio v${pkg.version}`,
        description: `A multi-purpose Discord Bot made by [Vex](https://github.com/TheVexatious/). With tons of commands for entertainment, This bot will keep your server filled with fun!\n\nEnjoying Xavio? Bring him to your server via [this link](https://discordapp.com/oauth2/authorize?client_id=${bot.config.clientID}&scope=bot).\n\nHead over to [**Xavio Headquarters**](${bot.config.guildInvite}) to hangout, get hit up with updates, and report bugs/suggestions!`,
        color: xavio.colors.DEFAULT,
        thumbnail: {
          url: bot.user.avatarURL
        },
        fields: [{
            name: "Library",
            value: `Eris v${pkg.dependencies.eris.substring(1)}`,
            inline: true
          },
          {
            name: "Runtime",
            value: `NodeJS v${process.versions.node}`,
            inline: true
          }
        ]
      }
    });
  }
};
