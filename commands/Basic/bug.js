module.exports = {
  description: "Forwards the given bug to the official #bugs channel in Xavio Headquarters",
  usage: "<detailed-description>",
  aliases: ["bugs"],
  cooldown: 2,
  process(bot, msg, suffix){
    if(suffix){
      bot.createMessage(bot.config.channels.bugs, {
        embed: {
          description: suffix,
          color: xavio.colors.RED,
          timestamp: bot.util.jsonDate(),
          footer: {
            text: "Bug Report"
          },
          author: {
            name: `${msg.author.username}#${msg.author.discriminator} (ID: ${msg.author.id})`,
            icon_url: msg.author.avatarURL
          }
        }
      }).then(()=>{
        msg.channel.createMessage("Successfully submitted your bug report! :thumbsup:");
      });
    }else{
      bot.util.correctUsage("bug", bot.config.prefix, this.usage, msg);
    }
  }
};
