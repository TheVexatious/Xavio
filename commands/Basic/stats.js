const path = require("path");
const pkg = require(path.join(xavio.baseDir, "package.json"));
const util = require("../../utils/util.js");

module.exports = {
  cooldown: 3,
  description: "Sends information about the bot",
  process(bot, msg) {
    const top = Object.keys(bot.commands).map(e => [e, bot.commands[e].execTimes]).sort((a, b) => b[1] - a[1]).slice(0, 10).map((e, i) => `${i + 1}. **${e[0]}** : ${e[1]}`).join("\n");
    msg.channel.createMessage({
      embed: {
        color: xavio.colors.DEFAULT,
        timestamp: util.jsonDate(),
        footer: {
          icon_url: bot.user.avatarURL,
          text: "Xavio  Statistics"
        },
        fields: [{
            name: "Xavio Version",
            value: `v${pkg.version}`,
            inline: true
          },
          {
            name: "Uptime",
            value: util.msFormat(bot.uptime),
            inline: true
          },
          {
            name: "Shard",
            value: `${bot.guildShardMap[msg.channel.guild.id]+1}/${bot.shards.size}`,
            inline: true
          },
          {
            name: "RAM Usage",
            value: `${process.memoryUsage().rss / 1024 / 1000}MiB`,
            inline: true
          },
          {
            name: "Command Usage",
            value: `${bot.commandsProcessed} commands used`,
            inline: true
          },
          {
            name: "Cleverbot Usage",
            value: `${bot.cleverResponses} responses`,
            inline: true
          },
          {
            name: "General Stats",
            value: `**${bot.guilds.size}** Guilds\n**${Object.keys(bot.channelGuildMap).length}** Channels\n**${bot.users.size}** Users`,
            inline: true
          },
          {
            name: "Top Commands",
            value: top,
            inline: false
          }
        ]
      }
    });
  }
};
