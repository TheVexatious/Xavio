module.exports = {
  description: "Forwards the given bug to the official #bugs channel in Xavio Headquarters",
  usage: "<detailed-description>",
  aliases: ["suggestion", "suggests"],
  cooldown: 2,
  process(bot, msg, suffix){
    if(suffix){
      bot.createMessage(bot.config.channels.suggestions, {
        embed: {
          description: suffix,
          color: xavio.colors.GREEN,
          timestamp: bot.util.jsonDate(),
          footer: {
            text: "Suggestion Report"
          },
          author: {
            name: `${msg.author.username}#${msg.author.discriminator} (ID: ${msg.author.id})`,
            icon_url: msg.author.avatarURL
          }
        }
      }).then(()=>{
        msg.channel.createMessage("Successfully submitted your suggestion report! :thumbsup:");
      });
    }else{
      bot.util.correctUsage("suggest", bot.config.prefix, this.usage, msg);
    }
  }
};
