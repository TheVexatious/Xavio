module.exports = {
  cooldown: 3,
  description: "Resumes the current playing song",
  process(bot, msg, suffix){
    const voice = bot.voiceConnections.get(msg.channel.guild.id);
    const getQueue = (g) => bot.plugins.music.getQueue(g);
    if(!voice || !voice.playing){
        bot.createMessage(msg.channel.id, "The bot is not playing any music!");
        return;
    }
    if(!voice.paused){
        bot.createMessage(msg.channel.id, "The bot is aleready playing any music!");
        return;
    }
    var q = getQueue(msg.channel.guild.id);
    voice.resume();
    bot.createMessage(msg.channel.id, `Resumed **${q[0].title}**`);
  }
};
