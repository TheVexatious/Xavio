module.exports = {
  cooldown: 5,
  usage: "<song name/link>",
  description: "Plays the song from the given name or link",
  process(bot, msg, suffix){
    if(!msg.member.voiceState.sessionID){
      return bot.createMessage(msg.channel.id, "You need to be in a voice channel!");
    }
    if(!suffix) return bot.util.correctUsage("play", bot.config.prefix, this.usage, msg);
    bot.joinVoiceChannel(msg.member.voiceState.channelID);
    bot.plugins.music.play(msg, bot.plugins.music.getQueue(msg.channel.guild.id), suffix);
  }
};
