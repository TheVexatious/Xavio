module.exports = {
  cooldown: 5,
  description: "Sends the name of the current playing song",
  process(bot, msg, suffix){
    const getQueue = (g) => bot.plugins.music.getQueue(x);
    const queue = getQueue(msg.channel.guild.id);
    if(queue.length == 0){
        bot.createMessage(msg.channel.id, "Queue is empty!");
    }else{
        bot.createMessage(msg.channel.id, `Currently playing: **${queue[0].title}** | by **${queue[0].requestor}**`);
    }
  }
};
