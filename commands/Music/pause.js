module.exports = {
  cooldown: 3,
  description: "Pauses the current playing song",
  process(bot, msg, suffix){
    const voice = bot.voiceConnections.get(msg.channel.guild.id);
    if(!voice){
        bot.createMessage(msg.channel.id, "The bot is not bonded to the current voice channel!");
        return;
    }
    const q = bot.plugins.music.getQueue(msg.channel.guild.id);
    voice.pause();
    bot.createMessage(msg.channel.id, `Paused **${q[0].title}**`);
  }
};
