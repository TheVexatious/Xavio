const Eris = require("eris");
const fs = require("fs");
const path = require("path");
const Command = require("../utils/commandClass.js");
const Cleverbot = require("cleverbot.io");
const utils = require("../utils/util.js");
const request = require("request");

let reload = require("require-reload")(require);

let logger = require("../utils/logger.js");

function loadSubdir(pathh, categories, bot){
	return new Promise((resolve, reject) => {
		let i = 0;
		let js = 0;
		let category = null;
		for(const cat in categories){
			if (categories[cat].name == path.basename(pathh)){
				category = categories[cat];
			}
		}
		fs.readdir(pathh, (err, files) => {
			if(err) bot.logger.error(err);
			for (let name of files) {
				i++;
				if (name.endsWith(".js")) {
					js++;
					name = name.replace(/\.js$/, "");
					try {
						const pth = path.join(`${bot.config.commandsDir || path.join(__dirname, "../commands/", path.basename(pathh))}`, `${name}.js`);
						bot.commands[name] = new Command(name, category, require(pth), bot.config, pth);
						bot.logger.logFileLoaded(`./commands/${path.basename(pathh)}/${name}.js`);
						if (files.length == i){
							resolve([i,files.length]);
						}
					} catch(e) {
						js--;
						bot.logger.logFileError(`./commands/${path.basename(pathh)}/${name}.js`, e);
						if (files.length == i){
							resolve([i,files.length]);
						}
					}
				}
			}
		});
	});
}

class Client extends Eris.Client {
	constructor(token, config) {

		super(token || config.token, config.options);

		this.token = token || config.token;
		this.config = config;

		this.commandsProcessed = 0;
		this.cleverResponses = 0;
		this.cleverbot = null;

		this.commands = {};
		this.events = {};
		this.plugins = {};

		this.queues = {};

		this.logger = new logger(true);
		this.util = utils;

		this.commandType = {};

		this.once("ready", () => {
			this.init().catch(this.logger.error);
			const games = ["get some x!help", "want x!info", "get an x!invite"];
			this.shards.forEach(shard => {
				let name = games[~~(Math.random() * games.length)];
				shard.editStatus(null, {name});
			});
			setInterval(()=> {
				this.shards.forEach(shard => {
					let name = games[~~(Math.random() * games.length)];
					shard.editStatus(null, {name});
				});
			}, 600000);

			setInterval(() => {
			    this.updateCarbon();
			    this.updateDBots();
			},3600000);
		});

		this.on("error", (error, shard) => {
			if (error) this.logger.error(`SHARD ${shard} | Error: ${error.stack || error.message || error}`);
		});
		this.on("disconnected", () => {
			this.logger.warn("Disconnected from Discord!");
		});
		this.on("shardReady", shard => {
			this.logger.info(`SHARD ${shard} | Ready!`);
		});
		this.on("shardDisconnect", (error, shard) => {
			if (error) this.logger.warn(`SHARD ${shard} | Disconnected: ${error.stack || error.message || error}`);
		});
		this.on("shardResume", shard => {
			this.logger.info(`SHARD ${shard} | Resumed`);
		});

	}

	initCommandCategories(categories) {
		Object.keys(categories).forEach(category => {
			this.commandType[category] = categories[category];
		});
	}

	processEvent(val) {
		if (val === "messageCreate") {
			this.on(val, (msg) => {
				if (msg.content.indexOf("reload") > -1) {
					if(typeof this.config.prefix == "string") {
						if (msg.content.startsWith(this.config.prefix + "reload") && msg.author.id == this.config.ownerId) {
								let suffix = msg.content.substring(this.config.prefix.length + 6).trim().toLowerCase();
								if (!this.commands[suffix]) return msg.channel.createMessage(`Command **${suffix}** does not exist!`);
								let neew;
								try {
										neew = reload(this.commands[suffix].location);
								} catch (e) {
										return this.logger.error(e);
								}
								delete this.commands[suffix];
								this.commands[suffix] = new Command(suffix, require(this.commands[suffix].location), this.config);
								msg.channel.createMessage(`Sucessfully reloaded command **${suffix}**!`);
								return;
						}
					} else {
						try{
							this.config.prefix.forEach((prefix, i, a) => {
								if (msg.content.startsWith(prefix + "reload") && msg.author.id == this.config.ownerId) {
										let suffix = msg.content.substring(prefix.length + 6).trim();
										if (!this.commands[suffix]) return msg.channel.createMessage(`Command **${suffix}** does not exist!`);
										let neew;
										try {
												neew = reload(this.commands[suffix].location);
										} catch (e) {
												return this.logger.error(e);
										}
										delete this.commands[suffix];
										this.commands[suffix] = new Command(suffix, require(this.commands[suffix].location), this.config);
										msg.channel.createMessage(`Sucessfully reloaded command **${suffix}**!`);
										throw new Error();
								} else if ((i + 1) == a.length) {
									this.events.messageCreate.execute(this, msg);
									throw new Error();
								}
							});
						}catch(e){}
					}
				} else {
					this.events.messageCreate.execute(this, msg);
					return;
				}
			});
		}

		if (val == "guildCreate") {
      this.on(val, (guild) => {
      	this.events.guildCreate.execute(this, guild);
    	});
    }

    if (val == "guildDelete") {
      this.on(val, (guild) => {
        this.events.guildDelete.execute(this, guild);
      });
    }
	}

	loadPlugins() {
		return new Promise((resolve, reject) => {
			fs.access("./plugins", fs.constants.R_OK | fs.constants.W_OK, (err) => {
				if (err) {
					return this.logger.custom("PLUGINS", "black.bgCyan", ": ", "white", "Plugins folder does not exist!", "cyan");
				} else {
					fs.readdir("./plugins/", (err, files) => {
						if (err) reject("Could not read plugins folder!");
						if (!files || files.length == 0) {
							return this.logger.custom("PLUGINS", "black.bgCyan", ": ", "white", "No files found in plugins folder!", "cyan");
						} else {
							let js = 0;
							let i = 0;
							for (let val of files) {
								i++;
								if (val.endsWith(".js")) {
									js++;
									val = val.replace(/\.js$/, ""); // replace the value which ends .js with nothing
									try {
										this.plugins[val] = require(`../plugins/${val}.js`);
										this.logger.logFileLoaded(`../plugins/${val}.js`);
										if (files.length == i) {
											this.logger.logEnd("PLUGINS", js, i);
											resolve();
										}
									} catch (e) {
										js--;
										this.logger.logFileError(`../plugins/${val}.js`, e);
										if (files.length == i){
											this.logger.logEnd("PLUGINS", js, i);
											resolve();
										}
									}
								}
							}
						}
					});
				}
			});
		});
	}

	loadEvents() {
		return new Promise((resolve, reject) => {
			fs.readdir((this.config.eventsDir || "./events/"), (err, files) => {
				if (err) reject("Could not read the events folder!");
				if (!files || files.length === 0) {
					reject("No files found in events folder!");
				}else{
					let js = 0;
					let i = 0;
					for (let name of files) {
						i++;
						if (name.endsWith(".js")) {
							js++;
							name = name.replace(/\.js$/, "");
							try {
								this.events[name] = require(`${this.config.eventsDir || "../events/"}${name}.js`);
								this.processEvent(name);
								this.logger.logFileLoaded(`./events/${name}.js`);
								if (files.length == i){
									this.logger.logEnd("EVENTS", js, i);
									resolve();
								}
							} catch(e) {
								js--;
								this.logger.logFileError(`./events/${name}.js`, e);
								if (files.length == i) {
									this.logger.logEnd("EVENTS", js, i);
									resolve();
								}
							}
						}
					}
				}
			});
		});
	}

	loadCommands() {
		return new Promise((resolve, reject) => {
			fs.readdir((this.config.eventsDir || "./commands/"), (err, folders) => {
				if (err) reject("Could not read the commands folder!");
				if (!folders || folders.length === 0) {
					reject("No files found in commands folder!");
				}else{
					let js = 0;
					let i = 0;
					let t = 0;
					for(const name in folders){
						loadSubdir(path.join((this.config.eventsDir || "./commands/"), folders[name]), this.commandType, this).then(int => {
							i++;
							js += int[0];
							t += int[1];
							if(folders.length == i){
								this.logger.logEnd("COMMANDS", js, t);
							}
						});
					}
					/*
					for (let name of files) {
						i++;
						if (name.endsWith(".js")) {
							js++;
							name = name.replace(/\.js$/, "");
							try {
								this.commands[name] = new Command(name, require(`${this.config.commandsDir || path.join(__dirname, "../commands/")}${name}.js`), this.config);
								this.logger.logFileLoaded(`./commands/${name}.js`);
								if (files.length == i){
									this.logger.logEnd("COMMANDS", js, i);
									resolve();
								}
							} catch(e) {
								js--;
								this.logger.logFileError(`./commands/${name}.js`, e);
								if (files.length == i){
									this.logger.logEnd("COMMANDS", js, i);
									resolve();
								}
							}
						}
					}*/
				}
			});
		});
	}

	initCleverbot(){
		return new Promise((resolve, reject) => {
			if(this.config.cleverbot) {
				if(this.config.cleverbotUser && this.config.cleverbotKey) {
					let cb = new Cleverbot(this.config.cleverbotUser, this.config.cleverbotKey);
					cb.setNick("xavio");
					cb.create((err, session) => {
						if (err) {
							this.logger.custom("WARN", "black.bgRed", ": ", "white", "Could not connect to cleverbot!", "yellow");
							this.config.cleverbot = false;
							resolve();
						} else {
							this.cleverbot = cb;
							resolve();
						}
					});
				} else {
					this.logger.custom("CLEVERBOT", "black.bgYellow", ": ", "white", "Cleverbot disabled because user key and api key not present in config!", "yellow");
					resolve();
				}
			} else {
				resolve();
			}
		});
	}

	sendReady(){
		return new Promise((resolve, reject) => {
			this.logger.logReady(`${this.user.username}#${this.user.discriminator}`,this.guilds.size);
		});
	}

	init() {
		return new Promise((resolve, reject) => {
			this.loadEvents()
				.then(this.loadCommands())
				.then(this.loadPlugins())
				.then(this.initCleverbot())
				.then(this.sendReady())
				.catch(this.logger.error);
			resolve();
		});
	}

	updateCarbon(){
		if (!key || !servers) return;
		request.post({
				"url": "https://www.carbonitex.net/discord/data/botdata.php",
				"headers": {"content-type": "application/json"}, "json": true,
				body: {
					"key": this.config.dbotsApiKey,
					"servercount": this.guilds.size
				}
			}, (e, r) => {
			    this.logger.debug(" Updated Carbon server count");
			    if (e) console.log("Error updating carbon stats: " + e);
			    if (r.statusCode !== 200) console.log("Error updating carbon stats: Status Code " + r.statusCode);
	        }
	    );
	}

	updateDBots(){
	    request.post(`https://bots.discord.pw/api/bots/${bot.user.id}/stats`, {
	        body: {
	            "server_count": this.guilds.size
	        },
	        headers: {
	            "Authorization": this.config.dbotsApiKey
	        },
	        json: true
	    }, (error, response, body) => {
	        this.logger.debug(" Updated Discord Bots server count");
	        if (error) console.log("Error updating Discord Bots stats: " + error);
	        if (response.statusCode !== 200) console.log("Error updating Discord Bots stats: Status Code " + r.statusCode);
	    });
	}

	/*
	Eris Client Extended methods:
	*/

	sendEmbed(msg, embed) {
		if(typeof embed == "string") {
			return this.createMessage(msg, {
				embed: {
					description: embed
				}
			});
		} else {
			return this.createMessage(msg, {
				embed: embed
			});
		}
	}

}


module.exports = Client;
