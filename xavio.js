const client = require("./lib/client.js");
const config = require("./config.json");
const bot = new client(null, config);

bot.initCommandCategories({
  BASIC: {
    name: "Basic",
    description: "Basic commands of the bot such as uptime, ping, ..etc",
    color: 7208811,
    requirements: () => true
  },
  NSFW: {
    name: "NSFW",
    description: "NSFW commands, commands that would work only in channels with \"nsfw\" in it's name",
    color: 14167341,
    requirements: (msg) => msg.channel.name.indexOf("nsfw") > -1,
    error: "You need to be in a channel with \"**nsfw**\" in it's name to execute commands in the NSFW category"
  },
  FUN: {
    name: "Fun",
    description: "Commands meant for entertainment",
    requirements: () => true
  },
  TEXT: {
    name: "Text",
    description: "Commands that are related to text manipulation",
    color: 11448238,
    requirements: () => true
  },
  MUSIC: {
    name: "Music",
    description: "Music commands",
    color: 16729274,
    requirements: (msg) => !!msg.channel.guild,
    error: "You can only execute music commands in a guild!"
  },
  SUPERADMIN: {
    hidden: true,
    name: "Superadmin",
    description: "Commands that can only be executed by the owner of the bot",
    color: 16240642,
    requirements: (msg, bot) => msg.author.id == bot.config.ownerId,
    error: "This command is restricted to the creator of the bot only"
  }
});
/*
setInterval(() => {
    bot.updateCarbon(config.carbonKey, bot.guilds.size);
    bot.updateDBots(config.dbotsApiKey, bot.guilds.size);
},600000);
*/
global.xavio = bot;

xavio.baseDir = __dirname;

xavio.colors = {
  DEFAULT: 8426481,
  RED: 14167341,
  GREEN: 7208811,
  ORANGE: 16759874,
  GREY: 11448238,
  PINK: 16729274,
  GOLD: 16240642
};

bot.connect();

/*
Handling unhandled promise rejections
*/
process.on("unhandledRejection", (reason, p) => {
    console.log("Possibly Unhandled Rejection at: Promise ", p, " reason: ", reason);
});
