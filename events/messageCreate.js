const utils = require("../utils/util.js");

function execCommand(bot, msg, suffix, plugins){
  bot.commands[cmd].exec(bot, msg, suffix, plugins);
}

module.exports = {
  execute(bot, msg) {
    if (!msg.channel.guild) {
      if (/(^https?:\/\/discord\.gg\/[A-Za-z0-9]+$|^https?:\/\/discordapp\.com\/invite\/[A-Za-z0-9]+$)/.test(msg.content)) {
        if (bot.config.clientID && bot.config.clientID !== "") msg.channel.createMessage("**Please use this to invite me to your server: ** https://discordapp.com/oauth2/authorize?client_id=" + config.clientID + "&scope=bot");
      }
    }
    if (msg.author.bot) {
      return;
    }

    const parser = utils.parsePrefix(bot.config.prefix, msg.content);

    if (msg.content.trim() == bot.user.mention) {
      // Send help
      // msg.channel.createMessage("Help shall be sent lol");
      if(bot.commands.hasOwnProperty("help")) bot.commands.help.exec(bot, msg, "", bot.plugins)
    } else if (parser.success) {
      const prefix = parser.prefix;
      let suffix = parser.parsed.trim();
      const cmdtxt = suffix.split(" ")[0].toLowerCase();
      suffix = suffix.trim().split(" ");
      suffix.splice(0, 1);
      suffix = suffix.join(" ").trim();

      // Check if command exists
      let cmd;
      for (const name in bot.commands) {
        if (name === cmdtxt || (bot.commands[name].aliases && bot.commands[name].aliases.includes(cmdtxt))) {
          cmd = name;
        }
      }

      if (cmd) {
        bot.commands[cmd].exec(bot, msg, suffix, bot.plugins);
      } else if (parser.mention && bot.config.cleverbot) {
        // Cleverbot
        bot.sendChannelTyping(msg.channel.id);
        bot.cleverbot.ask(suffix,(err, response) => {
          msg.channel.createMessage(response);
        });
      } else {
        if (bot.config.respondToInvalid) bot.createMessage(msg.channel.id, `Command '${cmd}' does not exist!`);
      }
    }
  }
};
